package oci.stx.springms;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringmsApplicationTests {

	@Test
	public void testReturnTrue() {
		assertEquals(1, 1, "must be true");
	}

	@Test
	public void testReturnTrueAnother() {
		assertEquals(0, 0, "must be true");
	}

}
